const fs = require('fs');
const request = require('request');
const path = require('path');
const redis = require('redis');
const bluebird = require('bluebird');
const MongoClient = require('mongodb').MongoClient;
const net = require('net');
const {Client} = require('pg');
const mysql = require('mysql');
const parseString = require('xml2js').parseString;
const xsd = require('libxmljs');
bluebird.promisifyAll(redis);

const setLoop = (fnName, fn, timeout, failureContinue, ...fnArgs) => {
    fn(...fnArgs)
        .then(() => {
            setTimeout(setLoop, timeout, fnName, fn, timeout, failureContinue, ...fnArgs);
        })
        .catch(() => {
            if (failureContinue) setTimeout(setLoop, timeout, fnName, fn, timeout, failureContinue, ...fnArgs);
        });
};

const errors = {
    "MAXIMUM_TIME_OUT": "MAXIMUM_TIME_OUT",
    "NOT_SUPPORTED_DATABASE": "NOT_SUPPORTED_DATABASE",
    "NOT_SUPPORTED_SERVICE": "NOT_SUPPORTED_SERVICE",
    "DATA_NOT_EXPECTED": "DATA_NOT_EXPECTED",
    "REQUEST_FAILURE": "REQUEST_FAILURE",
    "QUERY_ERROR": "QUERY_ERROR",
    "CONNECTION_ERROR": "CONNECTION_ERROR",
    "BAD_CONNECTION_STRING": "BAD_CONNECTION_STRING",
};

const jsonSuccess = (result) => {
    return {success: true, result}
};

const jsonError = (code) => {
    return {success: false, result: {code}};
};

const checkService = (service, maxTimeout, enableLog) => {

    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(jsonError(errors.MAXIMUM_TIME_OUT))
        }, maxTimeout);

        switch (service['serviceType']) {
            case 'db-service':
                switch (service['service']['type'][0]) {
                    case 'redis': {
                        let data = {
                            maxConnections: 0,
                            currentConnections: 0,
                        };
                        let redisClient = redis.createClient(service.service['connection-url'][0]);
                        redisClient.on("error", (err) => {
                            if (enableLog) {
                                console.log(err);
                            }
                            redisClient.end(true);
                            return resolve(jsonError(errors.CONNECTION_ERROR));
                        });
                        redisClient.pingAsync().then(result => {
                            if (result === 'PONG') {
                                // -- get info about max connection and
                                let promise1 = new Promise((resolve1) => {
                                    redisClient.configAsync("GET", "maxclients")
                                        .then((result) => {
                                            if (result && result.length === 2 && result[0] === "maxclients") {
                                                data.maxConnections = parseInt(result[1]);
                                            }
                                            return resolve1();
                                        })
                                        .catch(err => {
                                            if (enableLog) {
                                                console.log(err);
                                            }
                                            //-- config command is not recognized, change that elastic is being used
                                            //-- using default max connection
                                            data.maxConnections = 65000;
                                            return resolve1();
                                        });
                                });

                                let promise2 = new Promise((resolve1) => {
                                    redisClient.infoAsync()
                                        .then((result) => {
                                            let regex = new RegExp("connected_clients:(\\d+)", "g");
                                            let regexResult = regex.exec(result);
                                            if (regexResult && regexResult.length >= 2) {
                                                data.currentConnections = parseInt(regexResult[1]);
                                            }
                                            return resolve1();
                                        })
                                        .catch((err) => {
                                            if (enableLog) {
                                                console.log(err);
                                            }
                                            return resolve1();
                                        });
                                });

                                Promise.all([promise1, promise2]).then(() => {
                                    redisClient.end(true);
                                    return resolve(jsonSuccess(data));
                                });

                            } else {
                                redisClient.end(true);
                                return resolve(jsonError(errors.QUERY_ERROR));
                            }
                        }).catch(() => {
                            redisClient.end(true);
                            return resolve(errors.QUERY_ERROR);
                        });
                        break;
                    }

                    case 'mongodb': {
                        let data = {
                            maxConnections: 0,
                            currentConnections: 0,
                        };

                        MongoClient.connect(service.service['connection-url'][0], {
                            useNewUrlParser: true,
                        }, (err, client) => {
                            if (err) {
                                if (enableLog) {
                                    console.log(err);
                                }
                                return resolve(jsonError(errors.CONNECTION_ERROR));
                            }
                            //-- make request to get number of connections
                            client.db().command({"serverStatus": 1}, {}, (err, result) => {
                                if (!err) {
                                    data.currentConnections = parseInt(result["connections"]["current"]);
                                    data.maxConnections = data.currentConnections + parseInt(result["connections"]["available"]);
                                } else {
                                    if (enableLog) {
                                        console.log(err);
                                    }
                                }
                                client.close(true, () => {
                                    resolve(jsonSuccess(data));
                                });
                            });
                        });
                        break;
                    }

                    case 'postgres': {
                        let data = {
                            maxConnections: 0,
                            currentConnections: 0,
                        };
                        let client = new Client(service.service['connection-url'][0]);
                        client.connect((error) => {
                            if (!error) {
                                let query1 = new Promise((resolve1) => {
                                    client.query("SELECT sum(numbackends) as sum_numbackends FROM pg_stat_database", [], (err, result) => {
                                        if (!err && result.rows.length) {
                                            data.currentConnections = parseInt(result.rows[0]['sum_numbackends']);
                                        }
                                        resolve1();
                                    });
                                });

                                let query2 = new Promise((resolve1 => {
                                    client.query("SHOW max_connections", [], (err, result) => {
                                        if (!err && result.rows.length) {
                                            data.maxConnections = parseInt(result.rows[0]["max_connections"]);
                                        }
                                        resolve1();
                                    });
                                }));

                                Promise.all([query1, query2])
                                    .then(() => {
                                        client.end()
                                            .then(() => {
                                                return resolve(jsonSuccess(data));
                                            })
                                            .catch(() => {
                                                return resolve(jsonError(errors.QUERY_ERROR));
                                            });
                                    })
                                    .catch(() => {
                                        return resolve(jsonError(errors.QUERY_ERROR));
                                    })

                            } else {
                                if (enableLog) {
                                    console.log(error);
                                }
                                resolve(jsonError(errors.CONNECTION_ERROR));
                            }
                        });
                        break;
                    }

                    case 'mysql':
                        try {
                            let clientMysql = mysql.createConnection(service.service['connection-url'][0]);
                            clientMysql.connect((error) => {
                                const end = (err, result) => {
                                    if (!err) {
                                        clientMysql.end();
                                        resolve(jsonSuccess(result));
                                    } else {
                                        if (enableLog) {
                                            console.log(err);
                                        }
                                        resolve(jsonError(err));
                                    }
                                };
                                if (!error) {
                                    let data = {
                                        maxConnections: 0,
                                        currentConnections: 0,
                                    };
                                    let query1 = new Promise((resolve1) => {
                                        clientMysql.query("show status like 'Threads_connected'")
                                            .on('error', function () {
                                                end(errors.QUERY_ERROR);
                                            })
                                            .on('result', function (row) {
                                                switch (row['Variable_name']) {
                                                    case 'Threads_connected':
                                                        data.currentConnections = parseInt(row['Value']);
                                                        break;
                                                }
                                            })
                                            .on('end', function () {
                                                resolve1();
                                            });
                                    });

                                    let query2 = new Promise((resolve1) => {
                                        clientMysql.query("SHOW VARIABLES LIKE 'max_connections'")
                                            .on('error', function () {
                                                end(errors.QUERY_ERROR);
                                            })
                                            .on('result', function (row) {
                                                switch (row['Variable_name']) {
                                                    case 'max_connections':
                                                        data.maxConnections = parseInt(row['Value']);
                                                        break;
                                                }
                                            })
                                            .on('end', function () {
                                                resolve1();
                                            });
                                    });

                                    Promise.all([query1, query2])
                                        .then(() => {
                                            end(null, data);
                                        })
                                        .catch(() => {
                                            end(errors.QUERY_ERROR);
                                        })
                                } else {
                                    end(error);
                                }
                            });
                        } catch (e) {
                            return resolve(jsonError(errors.BAD_CONNECTION_STRING));
                        }
                        break;

                    default:
                        return resolve(jsonError(errors.NOT_SUPPORTED_DATABASE));
                }
                break;

            case 'http-service':
                const options = {
                    method: service.service['method'][0],
                    uri: service.service['connection-url'][0] + service.service['endpoint'][0],
                    json: true,
                    body: service.service['method'][0] === "POST" ? JSON.parse(service.service['data'][0]) : {}
                };
                request(options, (error, httpResponse) => {
                    if (error) {
                        if (enableLog) {
                            console.log(error);
                        }
                        return resolve(jsonError(errors.MAXIMUM_TIME_OUT));
                    }
                    if (enableLog) {
                        console.log(httpResponse.body);
                    }
                    if (httpResponse.statusCode === 200 || httpResponse.statusCode === 304) {
                        if (service.service['expect-regex'][0]) {
                            let regex = new RegExp(service.service['expect-regex'][0]);
                            let result = !!regex.exec(JSON.stringify(httpResponse.body));
                            if (!result) {
                                return resolve(jsonError(errors.DATA_NOT_EXPECTED))
                            }
                            return resolve(jsonSuccess());
                        }
                        return resolve(jsonSuccess());
                    }
                    return resolve(jsonError(errors.REQUEST_FAILURE));
                });
                break;

            case 'tcp-service':
                let clientTcp = new net.Socket();
                clientTcp.connect(parseInt(service.service['port'][0]), service.service['host'][0]);
                clientTcp.on('connect', () => {
                    clientTcp.destroy();
                    return resolve(jsonSuccess());
                });
                clientTcp.on('error', (err) => {
                    if (enableLog) {
                        console.log(err);
                    }
                    clientTcp.destroy();
                    return resolve(jsonError(errors.REQUEST_FAILURE));
                });
                clientTcp.on('timeout', () => {
                    clientTcp.destroy();
                    return resolve(jsonError(errors.MAXIMUM_TIME_OUT));
                });
                break;

            default:
                return resolve(jsonError(errors.NOT_SUPPORTED_SERVICE));
        }
    });
};

const agentProcess = async (configuration, maxTimeout, enableLog) => {

    let serviceChecked = [];

    let services = [];
    Object.keys(configuration['server'][0]['services'][0]).forEach(serviceType => {
        let serviceElement = configuration['server'][0]['services'][0][serviceType];
        serviceElement.forEach(s => {
            services.push({
                serviceType,
                service: s
            })
        });
    });

    let asyncServices = services.map(s => checkService(s, maxTimeout, enableLog));
    let asyncServiceResults = await Promise.all(asyncServices);
    for (let i = 0; i < asyncServiceResults.length; i++) {
        serviceChecked.push(Object.assign({
            name: services[i].service.name[0],
            status: asyncServiceResults[i]
        }, asyncServiceResults[i].success ? {last_contact: Date.now()} : {}));
    }

    if (enableLog) {
        console.log(Date.now(), asyncServiceResults);
    }

    let id = configuration['server'][0]['name'][0] || await new Promise(resolve => {
        request.get('http://instance-data/latest/meta-data/instance-id', (err, response) => {
            if (err) {
                if (enableLog) {
                    console.log(Date.now(), "Cannot resolve instance id: ", err);
                }
                return resolve("");
            }
            return resolve(response.body);
        });
    });

    return new Promise((resolve) => {
        const options = {
            method: 'POST',
            uri: configuration['health-check-server'][0] + "/healthcheck",
            body: {
                domain: configuration['domain'][0],
                group: configuration['group'][0],
                name: id,
                services: serviceChecked,
            },
            json: true,
        };

        if (enableLog) {
            console.log(Date.now(), "Posting to health check server: ", JSON.stringify(options));
        }
        request.post(options, (err, result) => {
            if (err) {
                if (enableLog) {
                    console.log(Date.now(), "Cannot post to health check server: ", err);
                }
                return resolve(false);
            } else {
                if (enableLog) {
                    console.log(Date.now(), "Return result from server: ", result.body);
                }
                return resolve(true);
            }
        });
    });
};

const HealthCheckAgent = function (configFilePath, interval, maxTimeout, enableLog) {
    //--- start checking process
    const xmlConfig = fs.readFileSync(configFilePath);
    const xmlSchema = fs.readFileSync(path.join(__dirname, "xsd", 'agent-config.xsd'));

    const xsdDoc = xsd.parseXmlString(xmlSchema);
    const configDoc = xsd.parseXmlString(xmlConfig);
    const result = configDoc.validate(xsdDoc);

    if (result === true) {
        parseString(xmlConfig, (err, result) => {
            if (err) {
                console.log('Agent exits. Error converting xml to json: ', err);
                process.exit();
            } else {
                setLoop('Health-check', agentProcess, interval, true, result['agent-config'], maxTimeout, enableLog);
                console.log('Health check is running.');
            }
        });
    } else {
        console.log('Agent exits. Configuration file not found or in invalid format.');
        process.exit();
    }
};

module.exports = {HealthCheckAgent};




