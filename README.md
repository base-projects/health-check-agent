Health check agent for checking several services.
Bundle with a XSD file to check for configuration.

How to use:

- install the package globally: `sudo yarn global add bapjsc-health-check-agent` (`sudo npm i -g` not work)
- create an XML config file. The XSD schema can be found embedded in the package at `lib/xsd/healchcheck.xsd`

```
<?xml version="1.0" encoding="UTF-8" ?>
<agent-config
        xmlns="https://xsd.bap.jp/healthcheck/agent"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="https://xsd.bap.jp/healthcheck/agent https://gitlab-new.bap.jp/template-projects/health-check-agent/raw/master/lib/xsd/agent-config.xsd"

>
...
</agent-config>
```
- run the health check process: `health-check-agent <config-file> <check-interval-in-milliseconds> <max-timeout-in-milliseconds> <show-log(true/false)>`
