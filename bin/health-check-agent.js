#!/usr/bin/env node

const path = require('path');
const HealthCheckAgent = require('../lib/app.js').HealthCheckAgent;
const version = require('../package').version;

//-- read from terminal
let configFilePath = process.argv[2];
let interval = process.argv[3];
let maxTimeout = process.argv[4];
let enableLog = process.argv[5];

if (["-v", "--version"].indexOf(configFilePath) >= 0){
    console.log(version);
    process.exit();
}


if (!configFilePath || !interval || !maxTimeout) {
    console.log("Usage:");
    console.log("health-check-agent <config file path> <check-interval-ms> <max-timeout-ms> <show-log(true/false)>");
    console.log("health-check-agent --version/-v");
    process.exit();
} else {
    HealthCheckAgent(path.isAbsolute(configFilePath)? configFilePath : path.join(process.cwd(), configFilePath), parseInt(interval), parseInt(maxTimeout), !!enableLog && enableLog === "true");
}